CREATE DATABASE cidb;

GRANT SELECT, INSERT ON cidb.* TO 'cidb'@'localhost' IDENTIFIED BY <todo>;

USE cidb;

CREATE TABLE events (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  idsite int(11) NOT NULL,
  lang varchar(128) DEFAULT NULL,
  ua varchar(128) DEFAULT NULL,
  category varchar(256) NOT NULL DEFAULT '',
  action varchar(256) NOT NULL DEFAULT '',
  name varchar(256) DEFAULT NULL,
  value double DEFAULT NULL,
  period_start timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  period_end timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  times int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE visits (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  idsite int(11) NOT NULL,
  lang varchar(128) DEFAULT NULL,
  ua varchar(128) DEFAULT NULL,
  action_name varchar(256) NOT NULL DEFAULT '',
  period_start timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  period_end timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  times int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);
