<?php

/**
 * Minimal implementation to show CleanInsights data stored in a database.
 * Use `cidb.sql` to create the needed database and `cidb.php` as the target
 * for any CleanInsights SDK to fill the database.
 *
 * Fill below configuration for allowed site IDs, CORS support and DB access,
 * or use a (shared) 'cleaninsights.ini' file.
 *
 * **Attention**: For production, this needs access control in front of it and TLS encryption.
 * See the provided `.htaccess` file for details. (You can use that for an Apache
 * Webserver, you will just need to create the `.htpasswd` file.)
 */

/**
 * Configuration
 */
$ALLOWED_IDSITES = [];

$PDO_DSN = '';
$PDO_USERNAME = '';
$PDO_PASSWORD = '';

// ********** end of configuration **********

// Override configuration with data from a configuration file, if available.

$CONF_FILE = 'cleaninsights.ini';

if (is_readable($CONF_FILE)) {
    $conf = parse_ini_file($CONF_FILE);

    if (array_key_exists('allowed_idsites', $conf) && $conf['allowed_idsites']) {
        if (is_array($conf['allowed_idsites'])) {
            $ALLOWED_IDSITES = $conf['allowed_idsites'];
        }
        else {
            $ALLOWED_IDSITES = [$conf['allowed_idsites']];
        }
    }

    if (array_key_exists('dsn', $conf) && $conf['dsn']) {
        $PDO_DSN = $conf['dsn'];
    }

    if (array_key_exists('username', $conf) && $conf['username']) {
        $PDO_USERNAME = $conf['username'];
    }

    if (array_key_exists('password', $conf) && $conf['password']) {
        $PDO_PASSWORD = $conf['password'];
    }
}



$selected_id = array_key_exists('idsite', $_GET) ? $_GET['idsite'] : $ALLOWED_IDSITES[0];

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>CleanInsights DB Analyzer</title>

		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
		      rel="stylesheet"
		      integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
		      crossorigin="anonymous">

		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">

		<link href="https://cdn.jsdelivr.net/npm/bootstrap-table@1.22.6/dist/bootstrap-table.min.css"
		      rel="stylesheet">

		<style>
			div.text-bg-danger {
                margin: 1em 0;
                padding: 1em;
				border-radius: 1ex;
			}

			form {
                margin-top: 1em !important;
			}

			h1 {
                margin-top: 1em;
			}

			h1 a {
				font-size: 33%;
			}
		</style>
	</head>
	<body>
		<div class="container">
<?php
$st1 = null;
$st2 = null;

try {
    $dbh = new PDO($PDO_DSN, $PDO_USERNAME, $PDO_PASSWORD);
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $st1 = $dbh->prepare('SELECT id, lang, ua, action_name, period_start, period_end, times FROM visits WHERE idsite = :idsite');
    $st1->bindValue(':idsite', $selected_id, PDO::PARAM_INT);
    $st1->execute();

    $st2 = $dbh->prepare('SELECT id, lang, ua, category, action, name, value, period_start, period_end, times FROM events WHERE idsite = :idsite');
    $st2->bindValue(':idsite', $selected_id, PDO::PARAM_INT);
    $st2->execute();
}
catch (Exception $exception) {
?>
	        <div class="text-bg-danger">
                <?php echo $exception->getMessage() ?>
	        </div>
<?php
}
?>
	        <form class="row row-cols-auto align-items-center">
		        <div class="col">
		            <label for="idsite" class="">Site ID</label>
		        </div>
		        <div class="col">
		            <select id="idsite" name="idsite" class="form-select">
<?php
    foreach ($ALLOWED_IDSITES as $id) {
?>
		                <option value="<?php echo $id ?>" <?php if ($id == $selected_id) { echo "selected"; } ?>><?php echo $id ?></option>
<?php
    }
?>
                    </select>
		        </div>

		        <div class="col">
			        <button type="submit" class="btn btn-primary">Reload</button>
		        </div>
	        </form>

			<h1 id="visits">
				Visits
				<a href="#events">Events</a>
			</h1>

	        <table class="visits table table-striped" data-search="true" data-pagination="true">
	            <thead>
	                <tr>
	                    <th data-sortable="true">Language</th>
	                    <th data-sortable="true">User-Agent</th>
	                    <th data-sortable="true">Action Name</th>
	                    <th data-sortable="true">Period Start</th>
	                    <th data-sortable="true">Period End</th>
	                    <th data-sortable="true">Times</th>
	                </tr>
	            </thead>
	            <tbody>
	            </tbody>
	        </table>

			<div>
				<canvas id="visits_chart"></canvas>
			</div>

			<h1 id="events">
				Events
				<a href="#visits">Visits</a>
			</h1>

	        <table class="events table table-striped" data-search="true" data-pagination="true">
	            <thead>
	            <tr>
	                <th data-sortable="true">Language</th>
	                <th data-sortable="true">User-Agent</th>
	                <th data-sortable="true">Category</th>
	                <th data-sortable="true">Action</th>
	                <th data-sortable="true">Name</th>
	                <th data-sortable="true">Value</th>
	                <th data-sortable="true">Period Start</th>
	                <th data-sortable="true">Period End</th>
	                <th data-sortable="true">Times</th>
	            </tr>
	            </thead>
	            <tbody>
	            </tbody>
	        </table>
		</div>

		<div>
			<canvas id="events_chart"></canvas>
		</div>

		<script src="https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
		        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
		        crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap-table@1.22.6/dist/bootstrap-table.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

		<script>
			let visit_data = [];
			let event_data = [];
<?php
    if ($st1) {
        $data = $st1->fetchAll(PDO::FETCH_ASSOC);

        if ($data) {
            $json = json_encode($data);

            if ($json) {
                echo "visit_data = $json;\n";
            }
        }
    }

	if ($st2) {
		$data = $st2->fetchAll(PDO::FETCH_ASSOC);

		if ($data) {
			$json = json_encode($data);

			if ($json) {
                echo "event_data = $json;\n";
			}
		}
	}
?>
            $('table.visits').bootstrapTable({
                columns: [
                    { field: 'lang' },
                    { field: 'ua' },
                    { field: 'action_name' },
                    { field: 'period_start' },
                    { field: 'period_end' },
                    { field: 'times' }
                ],
                data: visit_data
            });

			$('table.events').bootstrapTable({
				columns: [
                    { field: 'lang' },
                    { field: 'ua' },
                    { field: 'category' },
                    { field: 'action' },
                    { field: 'name' },
                    { field: 'value' },
                    { field: 'period_start' },
                    { field: 'period_end' },
                    { field: 'times' }
				],
				data: event_data
			});

            function transform(data, column) {
                let labels = [];
                let temp = [];

                $.each(data, (i, e) => {
                    const idx = labels.indexOf(e[column]);

                    if (idx < 0) {
                        labels.push(e[column]);

                        let x = {};
                        x[e.period_start + " – " + e.period_end] = e.times;

                        temp.push(x);
                    }
                    else {
                        temp[idx][e.period_start + " – " + e.period_end] += e.times;
                    }
                });

                let datasets = [];

                $.each(temp, (i, e) => {
                    $.each(e, (k, v) => {
                        let idx = datasets.findIndex((ds) => {
                            return ds.label === k
                        });

                        if (idx < 0) {
                            let d = $.map(labels, () => {
                                return 0
                            });
                            d[i] = v;

                            datasets.push({
                                label: k,
                                data: d
                            });
                        } else {
                            datasets[idx].data[i] = v;
                        }
                    });
                });

                return {
                    labels: labels,
	                datasets: datasets
                };
            }

            new Chart($('#visits_chart')[0], {
                type: 'bar',
                data: transform(visit_data, 'action_name')
            });

            new Chart($('#events_chart')[0], {
                type: 'bar',
                data: transform($.map(event_data, (e) => {
                    e['category_action'] = e.category + "/" + e.action;
                    return e;
                }), 'category_action')
            });
		</script>
    </body>
</html>


