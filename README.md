# Clean Insights Matomo Proxy (CIMP)

API to receive Clean Insights SDK data and preprocess it for Matomo's tracking API.

https://developer.matomo.org/api-reference/tracking-api

Goals:
- Clean Insights provides data which is older than a day. For this to work
  with Matomo, we need to use the `token_auth`. But this is way to powerful
  to hand it out to clients. CIMP will keep that around securely on the server instead.
  
- Clean Insights aggregates data over a period. To have useful reports
  in Matomo, that aggregated data needs to be pushed into Matomo as single
  data points. CIMP will do that, so the different Clean Insights SDKs don't
  need to implement that multiple times.
  
- When doing bulk uploading to the Matomo tracking API, a lot of data is repeated
  again and again, although it almost never changes. CIMP will take care of this
  and therefore reduce transfer size.
  
- Privacy preservation: Matomo records IP addresses and evaluates different HTTP
  headers. By using the proxy, we make sure, that Matomo isn't ever able to 
  accidentally record this information from a user. 
  
## Compatibility

Development is done against Matomo 3.14.1 and later. However, it *should* work with
all Matomo (and Piwik) versions supporting the tracking API v1.

## API

There is only one endpoint which can only be talked to via a POST request:

```json
POST https://matomo.example.org/cleaninsights.php

{
    "idsite": 1,
    "lang": "en_US", // OPTIONAL
    "ua": "iPhone", // OPTIONAL
    "visits": [ // OPTIONAL
        {
            "action_name": "Main/Settings/Some Setting",
            "period_start": 1599041128, // UNIX epoch timestamp
            "period_end": 1601633128, // UNIX epoch timestamp
            "times": 123
        }
    ],
    "events": [ // OPTIONAL
        {
            "category": "video-content",
            "action": "play",
            "period_start": 1599041128, // UNIX epoch timestamp
            "period_end": 1601633128, // UNIX epoch timestamp
            "times": 12
        }
    ]
}
```

Note: You will need to provide at least a visit or an event, otherwise nothing
gets recorded, although the script will return with 204 anyway.

### CORS support

CIMP supports Cross-Origin Resource Sharing through supporting OPTIONS preflight requests.
By default, all CORS requests are allowed. 
If you want to narrow that down, provide all allowed scheme/domain/port tuples
in the `$CORS` array at the top of [`cleaninsights.php`](cleaninsights.php) or 
in [`cleaninsights.ini`](cleaninsights.ini), if you prefer to use that.
 
### Formal Specification

JSON Scheme specification: https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schemas/cimp.schema.json

Generated JSON Scheme docs: https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schema-docs/README.md  


## Installation

- 2 Options:

  - *Easy to install* but also **easy to accidentally deinstall** during Matomo updates:
    On your server, copy this project into a subdirectory of the directory where
    Matomo is installed. (In the example above, we use 'ci'...)
  
  - More work but **more stable** install: Create another `VirtualHost` configuration
    on your web server installation, put [`cleaninsights.php`](cleaninsights.php) in its root folder.
    In [`cleaninsights.ini`](cleaninsights.ini), change `matomo_base_url` to point to your Matomo installation.
    Please consult the documentation of the web server you are using on how to set up virtual hosts.

- Make sure that your webserver honors the provided [`.htaccess`](.htaccess) file.
  E.g. Apache will have to have `AllowOverride` set to `All` for it to work. 
  *The goal is to have access forbidden for anything else then PHP files!*

- Create a `token_auth`. See https://matomo.org/faq/general/faq_114/

- Copy that token into [`cleaninsights.ini`](cleaninsights.ini) configuration file:

```ini
token_auth = ''
```

- Check the other configuration options documented in the ini file.

## Alternative Target

Since Matomo isn't everybody's flavor, we now also provide a minimal script named 
[`cidb.php`](cidb.php), which takes the data from all CleanInsights SDKs and just writes it into a
database.

### Installation

- Create a `VirtualHost` environment on your webserver, preferably with TLS encryption.
  (Most OS and browsers nowadays will not allow background connections to non-encrypted sites by default!)

- Install a SQL database and suitable PHP drivers. The script was developed with MariaDB, but any
  proper SQL database should do the job, since the queries used are pretty standard.

- Use the [`cidb.sql`](cidb.sql) script to create the database, user and tables.

- Set the `dsn`, `username` and `password` either in the script itself or in
  [`cleaninsights.ini`](cleaninsights.ini).

- With [`cleaninsights.php`](cleaninsights.php), Matomo tells us, which site IDs are valid,
  however, with [`cidb.php`](cidb.php), we need to configure this separately. Therefore: set the
  `allowed_idsites` array in [`cleaninsights.ini`](cleaninsights.ini).

- Make sure your webserver honors the provided [`.htaccess`](.htaccess) resp. duplicate
  that configuration by either doing something similar in your webserver config
  or by moving all non-public files into a directory outside your webserver's root.
  (Note: the last option will need some changes in [`cidb.php`](cidb.php).)

- For increased security in production, it is **highly recommended** that you use some
  rate limiting facility! Other measures, like user authentication are not an option for
  measurement tools, so you need some other way to stop evil actors from overflowing your
  database with crap data. Nginx provides rate limiting facilities. For Apache, there's
  multiple rate limiting plugins available (see e.g. https://stackoverflow.com/questions/131681/how-can-i-implement-rate-limiting-with-apache-requests-per-second),
  or you hide behind a bigger service provider like Cloudflare and use their security
  mechanisms. **Seriously don't use this without any additional security measures in production**!

### Visualize Captured Data

To visualize the captured data, you might look into tools like [Grafana](https://grafana.com).

We also provide an experimental visualizer with [`cidbanalyzer.php`](cidbanalyzer.php).

To make use of that, you will need to configure HTTP basic auth, as can be seen in [`.htaccess`](.htaccess).
(See e.g. https://www.howtogeek.com/devops/how-to-setup-basic-http-authentication-on-apache/ for instructions.)

**Never allow unrestricted access to [`cidbanalyzer.php`](cidbanalyzer.php)!**

*Privacy note*: [`cidbanalyzer.php`](cidbanalyzer.php) uses various libraries fetched from a public CDN!

## Author

Benjamin Erhart, berhart@netzarchitekten.com 
for the [Guardian Project](https://guardianproject.info).

## License

CIMP is available under the MIT license. See the LICENSE file for more info.
