<?php

/**
 * Minimal implementation to store CleanInsights data into a database.
 * Use `cidb.sql` to create the needed database.
 * Fill below configuration for allowed site IDs, CORS support and DB access,
 * or use a 'cleaninsights.ini' file.
 *
 * **Attention**: For production, this needs a tool in front of it,
 * which does rate limiting, to slow down any clever hackers who want to let
 * your database explode!
 */

/**
 * Configuration
 */
$ALLOWED_IDSITES = [];

// Cross-Origin Resource Sharing support. Add here all domains exactly as they
// are sent by the browser in the 'Origin' header. Keep empty to enable from everywhere.
// Example: 'https://example.com:3000'
$CORS = [];

$PDO_DSN = '';
$PDO_USERNAME = '';
$PDO_PASSWORD = '';

$DEBUG = false;

// ********** end of configuration **********

// Override configuration with data from a configuration file, if available.

$CONF_FILE = 'cleaninsights.ini';

if (is_readable($CONF_FILE)) {
    $conf = parse_ini_file($CONF_FILE);

    if (array_key_exists('allowed_idsites', $conf) && $conf['allowed_idsites']) {
        if (is_array($conf['allowed_idsites'])) {
            $ALLOWED_IDSITES = $conf['allowed_idsites'];
        }
        else {
            $ALLOWED_IDSITES = [$conf['allowed_idsites']];
        }
    }

    if (array_key_exists('cors', $conf) && $conf['cors']) {
        if (is_array($conf['cors'])) {
            $CORS = $conf['cors'];
        }
        else {
            $CORS = [$conf['cors']];
        }
    }

    if (array_key_exists('dsn', $conf) && $conf['dsn']) {
        $PDO_DSN = $conf['dsn'];
    }

    if (array_key_exists('username', $conf) && $conf['username']) {
        $PDO_USERNAME = $conf['username'];
    }

    if (array_key_exists('password', $conf) && $conf['password']) {
        $PDO_PASSWORD = $conf['password'];
    }

    if (array_key_exists('debug', $conf)) {
        $DEBUG = !!$conf['debug'];
    }
}

// Set up log.

if ($DEBUG) {
    ini_set('log_errors', 1);
    ini_set('error_log', 'ciapi.log');
}

$origin = array_key_exists( 'HTTP_ORIGIN', $_SERVER) ? $_SERVER['HTTP_ORIGIN'] : '';

// Safari needs this in both, an OPTIONS request and the subsequent POST request.
if (empty($CORS) || in_array($origin, $CORS, true)) {
    header("Access-Control-Allow-Origin: $origin");
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Headers: Content-Type');
}

// CORS preflight request support.
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    if (empty($CORS) || in_array($origin, $CORS, true)) {
        debug("CORS preflight request allowed for '$origin'.");

        http_response_code(200);
    }
    else {
        debug("CORS preflight request denied for '$origin'.");

        http_response_code(403);
    }

    exit(1);
}

// Only allowing POST here.
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    debug('Method not allowed');
    http_response_code(405);
    exit(1);
}

if (empty($ALLOWED_IDSITES)) {
    debug('ALLOWED_IDSITES not configured. Allow at least on site ID to start recording data!');
    http_response_code(500);
    exit(1);
}

$json = file_get_contents('php://input');
$data = json_decode($json);

// Obviously malformed POST body.
if (!$data || gettype($data) !== 'object' || !property_exists($data, 'idsite') || $data->idsite < 1) {
    debug('Malformed body. Last JSON error:' . json_last_error() . ' ' . json_last_error_msg());
    http_response_code(400);
    exit(1);
}

if (!in_array($data->idsite, $ALLOWED_IDSITES)) {
    debug("Site ID $data->idsite not allowed.");
    http_response_code(403);
    exit(1);
}

try {
    $dbh = new PDO($PDO_DSN, $PDO_USERNAME, $PDO_PASSWORD);
    $dbh->beginTransaction();

    $lang = property_exists($data, 'lang') ?  $data->lang : null;
    $ua = property_exists($data, 'ua') ? $data->ua : null;

    if (property_exists($data, 'visits') && gettype($data->visits) === 'array') {
        $sth = $dbh->prepare('INSERT INTO visits (idsite, lang, ua, action_name, period_start, period_end, times) VALUES (:idsite, :lang, :ua, :action_name, :period_start, :period_end, :times)');

        foreach ($data->visits as $visit) {
            if (!check($visit)
                || !property_exists($visit, 'action_name')
                || !$visit->action_name)
            {
                throw new Exception("evil", -666);
            }

            bind($sth, $data->idsite, $lang, $ua, $visit);

            $sth->bindValue(':action_name', $visit->action_name);

            $sth->execute();
        }
    }

    if (property_exists($data, 'events') && gettype($data->events) === 'array') {
        $sth = $dbh->prepare('INSERT INTO events (idsite, lang, ua, category, action, name, value, period_start, period_end, times) VALUES (:idsite, :lang, :ua, :category, :action, :name, :value, :period_start, :period_end, :times)');

        foreach ($data->events as $event) {
            if (!check($event)
                || !property_exists($event, 'category')
                || !$event->category
                || !property_exists($event, 'action')
                || !$event->action)
            {
                throw new Exception("evil", -666);
            }

            bind($sth, $data->idsite, $lang, $ua, $event);

            $sth->bindValue(':category', $event->category);
            $sth->bindValue(':action', $event->action);

            $name = property_exists($event, 'name') ? $event->name : null;
            $sth->bindValue(':name', $name);

            $value = property_exists($event, 'value') ? $event->value : null;
            $sth->bindValue(':value', $value);

            $sth->execute();
        }
    }

    $dbh->commit();
}
catch (Exception $exception) {
    if ($exception->getCode() == -666) {
        // Evil client sent malformed data.
        debug('Malformed body: missing required property.');
        http_response_code(400);
    }
    else {
        debug("Exception while interacting with DB: $exception");
        http_response_code(500);
    }

    exit(1);
}

debug('Successfully added data.');
http_response_code(204);

exit();

function check(stdClass $data_point): bool {
    return property_exists($data_point, 'period_start')
        && $data_point->period_start
        && property_exists($data_point, 'period_end')
        && $data_point->period_end
        && property_exists($data_point, 'times')
        && $data_point->times;
}

function bind(PDOStatement $sth, int $idsite, ?string $lang, ?string $ua, stdClass $data_point) {
    $start = new DateTime();
    $start->setTimestamp($data_point->period_start);

    $end = new DateTime();
    $end->setTimestamp($data_point->period_end);

    $sth->bindValue(':idsite', $idsite, PDO::PARAM_INT);
    $sth->bindValue(':lang', $lang);
    $sth->bindValue(':ua', $ua);
    $sth->bindValue(':period_start', $start->format('Y-m-d H:i:s'));
    $sth->bindValue(':period_end', $end->format('Y-m-d H:i:s'));
    $sth->bindValue(':times', $data_point->times, PDO::PARAM_INT);
}

function debug($message) {
    global $DEBUG;
    if (!$DEBUG) return;

    error_log(print_r($message, true));
}
